# build environment
FROM docker.io/node:13.12.0 as build

# RUN apt-get clean
# RUN rm -rf /var/lib/apt/lists/*
# RUN apt-get clean
# RUN apt-get update 
# RUN apt-get install git 

WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json ./
COPY package-lock.json ./
RUN npm ci
RUN npm install react-scripts@3.4.1 -g --silent
COPY . ./

RUN npm run build


# production environment
FROM docker.io/nginx:stable-alpine
COPY --from=build /app/build /usr/share/nginx/html
# new
COPY nginx/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
COPY .env .env