import React from "react";
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import { Grommet, Box,  } from "grommet";

import ConnectorJhub from "./features/connector/ConnectorJhub";

import Home from './features/home/Home'


const theme = {
  global: {
    colors: {
      brand: '#cc0000',
      back: "#292929",
      card: "#bfdbf7",
      accent: "#994650",
      ok: '#00C781',
    },
    font: {
      family: "Roboto",
      size: "18px",
      height: "20px",
    },
  },
};

function App() {
  return (
    <Grommet theme={theme} full>
      <Box fill align="center" justify="center" background="back">
      <Router>
                <Switch>
                <Route exact path='/' >
                      <Home />
                    </Route>
                    <Route exact path='/jupyterhub' >
                      <ConnectorJhub />
                    </Route>
                </Switch>
            </Router>
      </Box>
  
    </Grommet>
  );
}

export default App;
