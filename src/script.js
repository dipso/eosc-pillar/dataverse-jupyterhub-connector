const script = `#from fastapi import FastAPI
from flask import Flask, redirect, request
import requests
import json


#app = FastAPI()
app = Flask(__name__)
app.debug =True


dataverseDownloadFileUrl="https://data-test.jouy.inra.fr/api/access/datafile/:persistentId/?persistentId=doi:"
dataverseBaseUrl="https://data-test.jouy.inra.fr/api"
slash_url_escape_value="-"
slash="/"
two_point=":"

def getFileMetadata(filePid):
	completeResourceUrl=dataverseBaseUrl+"/files/:persistentId/metadata?persistentId=doi:"+filePid
	response=requests.get(completeResourceUrl)
	data=response.json()
	fileMetadata = DataverseFileMetadata()
	fileMetadata.fileName = data['label']

	if hasattr(data,'description'):
		fileMetadata.description = data['description']
	print("FileName "+fileMetadata.fileName+" and description : "+fileMetadata.description)
	return fileMetadata




def import_data_from_dataverse(filePid, datasetPid):
    #Extract PId
	if two_point in filePid:
		filePid_tab=filePid.split(two_point)
		if len(filePid_tab) >=2:
			filePid=filePid_tab[1]

	if two_point in datasetPid:
		datasetPid_tab=datasetPid.split(two_point)
		if len(datasetPid_tab) >=2:
			datasetPid=datasetPid_tab[1]

	filePidWithOutSlash=filePid
	datasetPidWithoutSlash=datasetPid

	if slash in filePid:
		filePidWithOutSlash=filePid.replace(slash,slash_url_escape_value)

	if slash in datasetPid:
		datasetPidWithoutSlash=datasetPid.replace(slash,slash_url_escape_value)

    #récuperer les metadata du fichier sur dataverse
	print("filePid is :"+filePid)
	fileMetadata=getFileMetadata(filePid)

    #download du fichier
	download_dataverse_file(filePid,fileMetadata.fileName)





#Download a file from 
def download_dataverse_file(filePid,fileName):
	finalDataverseUrl=dataverseDownloadFileUrl+filePid 
	response = requests.get(finalDataverseUrl, allow_redirects=True)
	open(fileName, 'wb').write(response.content)
	`
    
