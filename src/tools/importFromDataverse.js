
 const dataverseDownloadFileUrl=process.env.REACT_APP_DATAVERSE_URL + "/access/datafile/:persistentId?persistentId="
 const dataverseBaseUrl=process.env.REACT_APP_DATAVERSE_URL
 const dataverseToken=process.env.REACT_APP_DATAVERSE_TOKEN
 const slash_url_escape_value="-"
 const slash="/"
 const two_point=":"
  
 const token =  process.env.REACT_APP_JHUB
 let jhubApiHeaders = new Headers()
 jhubApiHeaders.append('Authorization', `Bearer ${token}`)

 export function blobToBase64(blob) {
    return new Promise((resolve, _) => {
      const reader = new FileReader();
      reader.onloadend = () => resolve(reader.result);
      reader.readAsDataURL(blob);
    });
  }

 export const getToken = async (name) => {

     var token

     const mybody = {
         usernames : [name]
     }

     // Create user if not already exists
     await fetch(`${process.env.REACT_APP_JHUB_URL}`, {
         method: 'POST',
         headers: jhubApiHeaders,
         body: JSON.stringify(mybody)
     })
     
     // create a new Jhub token for this user
     await fetch(`${process.env.REACT_APP_JHUB_URL}/${name}/tokens`, {
         method: 'POST',
         headers: jhubApiHeaders,
     })
     .then(resp =>token = resp.json())
     .then(data => {
         token = data.token
         
     })
     
     return token
 }
export const getFileMetadata = async (filePid) => {

    const complteRessouceUrl = dataverseBaseUrl + "/files/:persistentId/metadata?persistentId="+filePid
    var dataverseFileMetadata = {
        fileName : "",
        description : ''
    }
    const response = await fetch(complteRessouceUrl, {method: 'GET'})

    const data = await response.json()
    dataverseFileMetadata.fileName = data['label']

    if (data['description']) {
        dataverseFileMetadata.description = data['description']

    }
    console.log(dataverseFileMetadata)
    return dataverseFileMetadata
}

export const getDatasetMetadata = async (datasetPid) => {
    let dataverseHeaders = new Headers()
    dataverseHeaders.append('X-Dataverse-key', `${dataverseToken}`)
    const complteRessouceUrl = dataverseBaseUrl + "/datasets/:persistentId/?persistentId="+datasetPid
    const response = await fetch(complteRessouceUrl, {method: 'GET', headers: dataverseHeaders})
    const data = await response.json()
    const fileList = []

    data.data.latestVersion.files.map(file => fileList.push(file.dataFile.persistentId))

    return fileList
}

export const getFullMetadata = async (datasetPid) => {
   
    const complteRessouceUrl = dataverseBaseUrl + "/datasets/:persistentId/?persistentId="+datasetPid
    const response = await fetch(complteRessouceUrl, {method: 'GET'})
    const data = await response.json()
    
    return data
}


const dowloadDataverseFile = async (filePid) => {
    const finalDataverseUrl = dataverseDownloadFileUrl + filePid

    var dataToreturn = ''

    await fetch(finalDataverseUrl, {method : 'GET'})
    .then(resp =>resp.blob())
    .then(data => {
        dataToreturn = data
    })
   
    return dataToreturn
}


export const importFromDataverse = async (filePid) => {

    var trueFilePid = filePid
        
    if (filePid.includes(two_point)){
       var  filePidTab = filePid.split(two_point)
        if (filePidTab.length >=1){
            trueFilePid=filePidTab[1]
        }
    }

    var filePidWithOutSlash=trueFilePid

    if (filePidWithOutSlash.includes(slash)){
        filePidWithOutSlash.replace(slash, slash_url_escape_value)
    }
   const data = await dowloadDataverseFile(filePid)
    
 return data
}

export const isFileAlreadyUploaded = async (username, name, headers) => {
    const fileUrl = new URL(`${process.env.REACT_APP_JNOTEBOOK_URL}${username}/api/contents/`+name)
        var isFileUploaded = false
        await  fetch(fileUrl, {
            method: 'GET',
            headers: headers
        }).then(r => r.json())
        .then( data => {
            console.log(data)
            if (data.name === name){
                isFileUploaded = true
            }

        })

        return isFileUploaded
}

export const uploadFile = async (username, filename, file, headers ) => {
    const params = {
        path : '',
    }
    const fileUrl = new URL(`${process.env.REACT_APP_JNOTEBOOK_URL}${username}/api/contents/${filename}`)
    var fileBody = {
        "name":  filename,
        "type" : "file",
        "format":  "base64",
        "content":  file}

    Object.entries(params).forEach(([k, v]) => fileUrl.searchParams.append(k, v)); 
    fetch(fileUrl, {
        method: 'PUT',
        headers: headers ,
        body: JSON.stringify(fileBody)
    })

}
