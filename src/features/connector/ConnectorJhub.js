import React, { useEffect, useState } from 'react'

import {Box, Text, Image, Button, TextInput, Layer, RangeSelector, RangeInput} from "grommet"

import { useLocation } from 'react-router'

import GradientText from '../../components/GradientText'
import Card from '../../components/Card'
import ConcaveButton from '../../components/ConcaveButton'
import MiniButton from '../../components/MiniButton'

import jhub from '../../assets/images/jhub.png'
import Lottie from 'react-lottie'
import { Link } from 'react-router-dom'
import { FormPrevious } from 'grommet-icons'
import {
    importFromDataverse,
    getFileMetadata, 
    getDatasetMetadata,
    getFullMetadata, 
    getToken, 
    blobToBase64,
    isFileAlreadyUploaded,
    uploadFile} from '../../tools/importFromDataverse'
import { useLocalStorage } from '../../tools/localStorageHook'
 
import template from '../../CreerDataset.ipynb'
import templateUpdate from '../../AjouterFichiers.ipynb'

const ConnectorJhub =  () => {

    const query = new URLSearchParams(useLocation().search)

    const filePid= query.get("filePid") || `dataset`
    const datasetPid= query.get("datasetPid") || `doi:10.70112/V26AIH`

    const dataverseBaseUrl=process.env.REACT_APP_DATAVERSE_URL
    console.log(dataverseBaseUrl)

    const [username, setName] = useLocalStorage('username', '')
    const [go, setgo] = useState(false)
    const [ram, setram]= useState(2000)
    const [core, setcore] = useState(2)
    const [setup, setsetup] = useState(false)
    const [showOptions, setshowOptions] = useState(false)
    const [append,setappend] = useState(false)
    const [fileAlredyThere, setfileAlreadyThere] = useState(false)

    const token =  process.env.REACT_APP_JHUB
    const maxRam = process.env.REACT_APP_MAX_RAM
    const maxCore = process.env.REACT_APP_MAX_CORE

    const coreOptions = Array.from({length: maxCore}, (_, i) => i + 1)

    let jhubApiHeaders = new Headers()
    jhubApiHeaders.append('Authorization', `Bearer ${token}`)


    const uploadTemplate = async (jnotebookApiHeaders) => {

        const toBlobAndSend= async (data, name) => {

            // console.log(fileToUpload)   
            var fileToUpload =new Blob([JSON.stringify(data)])
            fileToUpload = await blobToBase64(fileToUpload)
            fileToUpload = fileToUpload.split(',')[1]


             var fileBody = {
                "name": name,
                "type" : "file",
                "format":  "base64",
                "content":fileToUpload}

                console.log(data,fileBody)
            const params = {
                path : '',
            }

            const fileUrl = new URL(`${process.env.REACT_APP_JNOTEBOOK_URL}${username}/api/contents/${fileBody.name}`)
            Object.entries(params).forEach(([k, v]) => fileUrl.searchParams.append(k, v)); 
            fetch(fileUrl, {
                            method: 'PUT',
                            headers: jnotebookApiHeaders ,
                            body: JSON.stringify(fileBody)
                        })

        }

       

       const templateAlreadyUploaded = await isFileAlreadyUploaded(username,'Creer dataset.ipynb', jnotebookApiHeaders )

        !templateAlreadyUploaded && await fetch(template)
        .then(r => r.json())
        .then(data => {
            toBlobAndSend(data, "Creer dataset.ipynb")
            
        })

        var templateUpdateAlreadyUploaded = await isFileAlreadyUploaded(username,'AjouterFichiers.ipynb',jnotebookApiHeaders )        

        !templateUpdateAlreadyUploaded && await fetch(templateUpdate)
        .then(r => r.json())
        .then(data => {
            toBlobAndSend(data, "AjouterFichiers.ipynb")
            
        })


    }
    const loadFileOptions = async (file, option ) => {
        const tokenJhub = await getToken(username)

        let jnotebookApiHeaders = new Headers()
        jnotebookApiHeaders.append('Authorization', `token ${tokenJhub}`)
        const metadata = await getFileMetadata(file)
        
        const content = await importFromDataverse(file)
        var fileToUpload = await blobToBase64(content)
        fileToUpload = fileToUpload.split(',')[1]

        option 
            ? await uploadFile(username, "Copy" +"-"+metadata.fileName , fileToUpload, jnotebookApiHeaders) :
            await uploadFile(username, metadata.fileName , fileToUpload, jnotebookApiHeaders)

        window.open(`${process.env.REACT_APP_JNOTEBOOK_URL}${username}`).focus()
    }


    const [isLoading, setIsLoading] = useState(false)

    const [isStarting, setisStarting] = useState(false)

    const [hasStarted, sethasStarted] = useState(false)

    const compute = async () =>  {
        setgo(true)
        setIsLoading(true)

        console.log('resources', maxCore, maxRam)

        const tokenJhub = await getToken(username)

        let jnotebookApiHeaders = new Headers()
        jnotebookApiHeaders.append('Authorization', `token ${tokenJhub}`)

        const params = {
            path : '',
        }
       
        //Get the file from dataverse
        const loadFile = async (file, jnotebookApiHeaders) => {
            const metadata = await getFileMetadata(file)
              
            const content = await importFromDataverse(file)

            console.log(content)

            var fileToUpload = await blobToBase64(content)
            fileToUpload = fileToUpload.split(',')[1]

            // start a new Jupyter Hub if this user have not one running already
            // then upload the dataverse file to the server
            // then redirect the user to the server

            const uploadCurrentFile = async() => {
                var fileAlreadyUploaded = await isFileAlreadyUploaded(username,metadata.fileName , jnotebookApiHeaders)
                setfileAlreadyThere(fileAlreadyUploaded)
                console.log(fileAlreadyUploaded)
                fileAlreadyUploaded && setshowOptions(true)

                !fileAlreadyUploaded && await uploadFile(username,metadata.fileName, fileToUpload, jnotebookApiHeaders )
                setIsLoading(false)
                !fileAlreadyUploaded && window.open(`${process.env.REACT_APP_JNOTEBOOK_URL}${username}`).focus()

            }

            var startServer = async () => {

                const serverBody = {
                    
                    "default": true,
                    "kubespawner_override": {
                        "cpu_guarantee": core,
                        "cpu_limit": core,
                        "mem_guarantee": ram.toString() +"M",
                        "mem_limit": ram.toString() +"M"
                    }
                
                }

                await fetch(`${process.env.REACT_APP_JHUB_URL}/${username}/server`, {
                    method: 'POST',
                    headers: jhubApiHeaders,
                    body: JSON.stringify(serverBody)
                })
                .then((response) => {
                    if (response.status === 202){
                        setisStarting(true)

                        setTimeout(() => {
                            startServer()
                        }, (5000));
                    
                    }
                    else {
                        sethasStarted(true)

                        uploadCurrentFile()
                       

                    }
                })
                .then(() => {
                    uploadTemplate(jnotebookApiHeaders) 
                })
            }

            setup && !showOptions && startServer()
        }


        if (filePid !== 'dataset'){
           
            loadFile(filePid, jnotebookApiHeaders)
            
        }else {

            const fileList = await getDatasetMetadata(datasetPid)
            console.log(fileList)
            
            for (const file of fileList){
                await loadFile(file, jnotebookApiHeaders)
            }
            
           setup && window.open(`${process.env.REACT_APP_JNOTEBOOK_URL}${username}`).focus()

        } 
       
    }

    useEffect(()=> {
        if (username !== ''){
            compute()
        }
    }, [setup])

    return (
        <Box align="center">
            <GradientText>EOSC-Pillar Jupyter Hub connector</GradientText>

            <Box width="small">
                   <Image src={jhub} fit='contain' />
            </Box>

            {go  ? (
                <Box align='center'>

                    <Box align="center" margin="small">
                        <Text>filePid : {filePid}</Text>
                        <Text>datasetPid: {datasetPid}</Text>    
                    </Box>

                    <Box margin="small"  align="center" gap="small" justify='center'>
                        <Text>RAM : {ram} M</Text>
                        <RangeInput max={maxRam} setp={100} value={ram} onChange={event => setram(event.target.value)}/>
                        <Text>Cores :</Text>
                        <Box direction='row' align="center" justify="center" gap='small'>
                        {coreOptions.map(option => (
                            <Box key={option} align="center" justify="center" gap="small">
                                {core === option ? (
                                    <MiniButton round="full" width='20px' height='20px' />
                                ) : (
                                    <ConcaveButton round="full" width='20px' height='20px' onClick={() => setcore(option)} />
                                )}
                                <Text>{option}</Text>
                            </Box>
                            ))}
                            </Box>
                    </Box>

                    <ConcaveButton margin="small" align='center' width="xsmall" round="small" pad="small" onClick={()=> {
                        if (username !== ''){

                            setsetup(true)
                        }
                    }}>Save</ConcaveButton>
                    
                    {isLoading && setup && (
                        <Lottie
                            options={{
                                loop: true, 
                                path: 'https://assets3.lottiefiles.com/packages/lf20_rPGSco.json'
                            }}
                            height={100}
                            width={100}
                            />
                    )}
                    
                    {setup && <Text>Starting a Jupyter Hub server...</Text>}
                    {isStarting && setup &&  (
                        <Text margin="small">Serveur has started, uploading your data...</Text>
                    )}
                    {hasStarted && setup && (
                        <Text margin="small">Waiting for redirection...</Text>
                    )}

                </Box>
            ) : (
                <Box gap="medium" align="center">

                    <Text>Please enter your JupyterHub username</Text>
                    <Card round='small' pad="small" align='center' justify="center">
                        <Box  align='center' justify="center">
                            <TextInput placeholder="username..."  plain value={username} onChange={event => setName(event.target.value)} />
                        </Box>
                    </Card>

                    <ConcaveButton align='center' width="xsmall" round="small" pad="small" onClick={()=> {
                        if (username !== ''){

                            compute()
                        }
                    }}>Save</ConcaveButton>


                </Box>
            )}

            <Box>
                {showOptions && (
                    <Layer
                    onEsc={() => setshowOptions(false)}
                    onClickOutside={() => setshowOptions(false)}
                    >
                        <Box pad="medium" gap="small">
                            <Text>A file with this name already exists on your Jupyter Hub server : </Text>
                            <Button label="Overwrite file" onClick={() => {
                                setshowOptions(false)
                                
                                //use function 
                              
                                setappend(false)
                                loadFileOptions(filePid, false)
                                }} />
                            <Button label="Keep both file" onClick={() => {
                                setshowOptions(false)

                                //use function 

                                setappend(false)
                                loadFileOptions(filePid, true)
                                }} />
                            <Button label="Do nothing" onClick={() => {
                                setshowOptions(false)
                                setsetup(false)}} />

                        </Box>
                    </Layer>
                )}
                </Box>

           
           
            <Box margin={{top: "large"}}>
                 <Link to='/'>
                     <Button icon={<FormPrevious color='orange' />} onClick={() => setName('')} />
                 </Link>
             </Box>
        </Box>
    )
}

export default ConnectorJhub;