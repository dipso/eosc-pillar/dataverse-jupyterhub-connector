import React from 'react'

import { Box, Image } from 'grommet'
import GradientText from '../../components/GradientText'

import Lottie from 'react-lottie'


import dataverse from '../../assets/images/dataverse.png'
import jhub from '../../assets/images/jhub.png'

import { Link } from 'react-router-dom'

const Home = () => {

    return (
        <Box align="center">
            <GradientText>Welcome to the EOSC-Pillar Jupyter Hub Connector</GradientText>

            <Box margin="medium" align="center" >
                
                <Box width="small">
                   <Image src={dataverse} fit='contain' />
                </Box>
                
                <Lottie 
                options={{
                    loop: true, 
                    path: 'https://assets9.lottiefiles.com/packages/lf20_xj2xkswn.json'
                }}
                height={200}
                />
                
                <Box direction='row' align='center' justify='center' >
                    
                    <Link to='/jupyterhub'>
                       <Box width="small">
                          <Image src={jhub} fit='contain' />
                       </Box>
                    </Link>
                   
                </Box>
            </Box>
        </Box>
    )
}

export default Home;