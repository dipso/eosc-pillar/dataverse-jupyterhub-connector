# Description

The Dataverse-to-JupyterHub Data Transfer Connector is a proof of concept tool aimed at facilitating the transfer of data from a Dataverse repository to cloud-based analysis platforms like JupyterHub. This solution operates within the context of the EOSC Pillar project, seeking to explore the possibilities of data interoperability within the European Open Science Cloud (EOSC) infrastructure.

By enabling the transfer of datasets from Dataverse to JupyterHub, this connector demonstrates the potential for streamlined data analysis and exploration. While still in the proof of concept phase, it showcases the prospect of a more integrated research environment, fostering efficient data utilization and collaborative research practices.

Key features :

- *File and datasets 2 way transfer* : based on the Dataverse's External Tool feature, as well as the Dataverse and JupyterHub API, the connector enables easy transfer from any instance of Dataverse to a cloud hosted JupyterHub server. Pre-loaded scripts enables transfer back to the Dataverse repository to promote data reutilization and sharing.

- *Lightweight and customisable* : the connector is a client side web application that is easily deployable on a number of technologies, notably modern integration and deployment systems such as Kubernetes. Simple configuration variables manage targets (Dataverse and JupyterHub instances), and authentification systems.

-  *Early-stage Integration* : Showcases the initial steps toward integrating Dataverse repositories with cloud-based analytical platforms, demonstrating the viability of future data utilization models.

At this stage, the application can manage small to medium size files and uses open access Dataverse API endpoints.
Large file support (>1gb) and signed Dataverse endpoints support are comming soon.

![](src/assets/images/schema.png)

### To setup the environnement variables, please follow this template:

```

REACT_APP_JHUB='{{HERE_YOUR_JUPYTER_HUB_TOKEN}}'

REACT_APP_JHUB_URL='https://{{HERE YOUR JUPYTER HUB BASE URL}}/hub/api/users'

REACT_APP_JNOTEBOOK_URL='https://{{HERE YOUR JUPYTER HUB BASE URL}}/user/'

REACT_APP_DATAVERSE_URL='https://{{HERE YOUR DATAVERSE BASE URL}}/api'

REACT_APP_MAX_RAM = {{Max ram in Mo }}

REACT_APP_MAX_CORE = {{Max Core number}}

```

You can then create a new Gitlab environnement file with this template via Settings -> CI/CD -> Variables

Remember to add this line at the end od the Dockerfile for the environnement variables to be included in the image

```
COPY .env .env

```

### To allow CORS on Jupyter Hub's side:

On your Jupyter Hub root folder, edit /config/jupyterhub_config.d/spawner.py to add the following lines :

```
origin = '{{Your connector URL}}'

c.Spawner.args = [f'--NotebookApp.allow_origin={origin}']
c.JupyterHub.tornado_settings = {
    'headers': {
        'Access-Control-Allow-Origin': origin,
    },
}

```

### Deployment

The deployment of this connector is done via gitlab CI/CD pipeline.
The pipeline has 2 stages, build and deploy. In the build stage, we create the docker image of the node application and we push it to gitlab registry.
In the deploy stage, we use the build image to deploy an Helm chart to kubernetes cluster.
The pipeline is executed with gitlab runner installed in orion kubernetes cluster. The url of the deployment appears in the end of the logs of successful execution of the pipeline.

This connector can be deploy in a simpler environnement as long as the env files are accessible.

### Dataverse External Tools

Once your connector is online, make sure to update the Dataverse External Tools on your dataverse server by adding at tool pointing to your connector :

https://{{your connector domain name}}/jupyterhub/?filePid={{the filePid from the external tools query}}

This connector comes with a script to automate the creation of an external tool for this connector for all files type. As dataverse doesn't allow generic types as an input, we have to create a JSON entry for every accepted file types.

This script (loadTools.py) should be ran on your dataverse server with it's json list (tools.json).

### Dataset scope on Dataverse

You can activate this connector to work on the datasets scope. It will upload all the dataset files on your jupyterHub.

To do so you need to add another external tool to your Dataverse with the following model :

```
{
    "displayName": "Jupyter Hub ",
    "description": "Jupyter Hub tool for dataset upload",
    "scope": "dataset",
    "types": ["explore"],
    "toolUrl": "https://jupyterhub-connector.k.orion.cloud.inrae.fr/jupyterhub/",
    "toolParameters": {
      "queryParameters": [
        {
          "datasetPid": "{datasetPid}"
        }
      ]
    }
  }
```
