import json
import requests


f = open('tools.json', 'r')


url = 'http://localhost:9080/api/admin/externalTools'
headers = {'Content-type': 'application/json'}

list = json.load(f)
for i in list:
    with open('jupyterhub-file.json', 'w') as o:
        o.write(json.dumps(i))
    r = requests.post(url, data=open(
        'jupyterhub-file.json', 'rb'), headers=headers)
    print(r.text)
